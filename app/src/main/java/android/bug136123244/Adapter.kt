package android.bug136123244

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

class Adapter : ListAdapter<Model, Adapter.ViewHodler>(DIFF_UTIL) {
    private companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<Model>() {
            override fun areItemsTheSame(oldItem: Model, newItem: Model) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Model, newItem: Model) = oldItem == newItem
        }
    }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHodler(
        LayoutInflater.from(parent.context).inflate(
            android.R.layout.simple_list_item_2,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ViewHodler, position: Int) {
        holder.text1.text = getItem(position).name
        holder.text2.text = getItem(position).id.toString()
    }

    override fun getItemId(position: Int) = getItem(position).id

    inner class ViewHodler(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        val text1: TextView = containerView.findViewById(android.R.id.text1)
        val text2: TextView = containerView.findViewById(android.R.id.text2)

        val itemDetails: ItemDetailsLookup.ItemDetails<Long>
            get() = object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getSelectionKey() = itemId
                override fun getPosition() = adapterPosition
            }
    }
}