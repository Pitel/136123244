package android.bug136123244

data class Model(
    val id: Long,
    val name: String
)