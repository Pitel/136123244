package android.bug136123244

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import androidx.recyclerview.selection.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = Adapter()
        recycler.setHasFixedSize(true)
        recycler.adapter = adapter
        adapter.submitList(
            listOf(
                Model(10, "ABC"),
                Model(20, "DEF"),
                Model(30, "UVW"),
                Model(40, "XYZ")
            )
        )

        val tracker = SelectionTracker.Builder(
            "136123244",
            recycler,
            StableIdKeyProvider(recycler),
            object : ItemDetailsLookup<Long>() {
                override fun getItemDetails(e: MotionEvent) =
                    recycler.findChildViewUnder(e.x, e.y)?.let { view ->
                        (recycler.getChildViewHolder(view) as Adapter.ViewHodler).itemDetails
                    }
            },
            StorageStrategy.createLongStorage()
        )
            .withSelectionPredicate(SelectionPredicates.createSelectSingleAnything())
            .build()

        handler.postDelayed(object : Runnable {
            override fun run() {
                selection.text = tracker.selection.joinToString()
                handler.postDelayed(this, 100)
            }
        }, 100)

        rename.setOnClickListener {
            adapter.submitList(
                adapter.currentList.map { it.copy(name = it.name.reversed()) }
            )
        }
    }
}
