1. Long press *ABC 10*.
2. *10* is displayed at the bottom.
3. Long pres *DEF 20*.
4. *20* is displayed at the bottom.
5. Everything now works as expected.
5. Click *Rename* button.
6. Item's names are reversed, ids stays the same.
7. Long press *ABC 10*.
8. Crash!